# ta_espe

TA ESPE, short for Techage Energy, Storage and Product Extension, adds some new things to Techage. (Warning, much still planning!)

# License

In `LICENSE_MEDIA` is the license for all media.(all files in folder sounds, textures if exist and all other media). Special thanks to [JoSto](https://github.com/joe7575/) and all other contributor on techage. nuclear/rtg.lua has some things from [techage](https://github.com/joe7575/techage/). Thankyou for [JoSto](https://github.com/joe7575/) who made the [markdown2lua.py](https://github.com/joe7575/ta4_jetpack/blob/master/markdown2lua.py) which i modified a bit(licensed under GNU GPL version 3 or later). All other things are under the license in `LICENSE`.
