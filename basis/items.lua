ta_espe.items = {}

if minetest.global_exists("technic") then
	ta_espe.items.lead_lump = "technic:lead_lump"
	ta_espe.items.lead_ingot = "technic:lead_ingot"
	ta_espe.items.lead_block = "technic:lead_block"
	ta_espe.items.stone_with_lead = "technic:mineral_lead"
else
	ta_espe.items.lead_lump = "ta_espe:lead_lump"
	ta_espe.items.lead_ingot = "ta_espe:lead_ingot"
	ta_espe.items.lead_block = "ta_espe:leadblock"
	ta_espe.items.stone_with_lead = "ta_espe:stone_with_lead"
end
