techage.add_to_manual('DE', {
  "1,Techage ESPE",
  "2,Über",
  "3,Erze und Mineralien",
  "4,Hafnium",
  "4,Blei",
  "4,Plutonium",
  "4,Tantal",
  "4,Tantalhafniumcarbid",
  "4,Uran",
  "4,Wolfram",
  "4,Wolframcarbid",
}, {
  "TA ESPE\\, kurz Techage Energie\\, Speicherung und Produkt Erweiterung\\, fügt einges neues zu Techage hinzu. (Warnung\\, vieles noch Planung!)\n"..
  "\n",
  "",
  "TA ESPE fügt einige neue Items hinzu:\n"..
  "\n"..
  "  - Hafnium - Ein seltenes aber wichtiges Material\n"..
  "  - Blei - Ein schweres Metall welches häufiger vorkommt\n"..
  "  - Plutonium - Abfallprodukt von frühen Atomreaktoren\\, verwendbar in RTG-Baterien\n"..
  "  - Tantal - Nötig für Kondensator-Akku und speziel Stoffe\n"..
  "  - Tantalhafniumcarbid - Material mit hohen Schmelzpunkt.\n"..
  "  - Uran - Radioaktiver Stoff\\, hocheffizient für Atomreaktoren\n"..
  "  - Wolfram - Ein Erz mit hohen Siedepunkt\n"..
  "  - Wolframcarbid - Eine Alternative zu Diamanten\n"..
  "\n",
  "Hafnium wird verwendet um Steuerstäbe für Atomgeneratoren herzustellen\\, aber auch um Tantalhafniumcarbid herzustellen.\n"..
  "\n",
  "Blei ist ein schweres Metall. Es eignet sich auch als Abschirmung bei Nutzung von Radioaktivität. Aufgrund seiner Häufigkeit sollte es keine Probleme machen. Sofern nicht anders angegeben zerfallen Materialien die Radioaktiv sind in Blei.\n"..
  "\n",
  "Ultra selten\\, kommt jedoch als Abfallprodukt in Atomreaktoren raus. Kann in RTG-Baterien benutzt werden um Strom zu gewinnen\n"..
  "\n",
  "Aus Tantal lassen sich Kondensator-Akkus herstellen\\, die mehr Strom als gewöhnliche Akkus abspeichern können\n"..
  "\n",
  "Tantalhafniumcarbid intermetallische Verbindung aus Tantal\\, Hafnium und Kohlenstoff. Durch seinen hohen Schmelzpunkt kann es für Röhren verwendet werden die Gas mit hohen Temperaturen durchleiten können.\n"..
  "\n",
  "Uran ist der Brennstoff für Atomkraftwerke\\, die vor allen Dingen in weiten Formen Mengen an Strom geben.\n"..
  "\n",
  "Wolfram hat einen sehr hohen Siedepunkt.\n"..
  "\n",
  "Unter anderem eine Alternative zu Diamanten. Außerdem kann man auch Werkzeuge herstellen die mit einer niedrigen Wahrscheinlichkeit Ölröhren beim Abbau wieder geben.\n"..
  "\n",
}, {
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
}, {
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
})
