# Techage ESPE

TA ESPE, kurz Techage Energie, Speicherung und Produkt Erweiterung, fügt einges neues zu Techage hinzu. (Warnung, vieles noch Planung!)

## Über

### Erze und Mineralien

TA ESPE fügt einige neue Items hinzu:

- Hafnium - Ein seltenes aber wichtiges Material
- Blei - Ein schweres Metall welches häufiger vorkommt
- Plutonium - Abfallprodukt von frühen Atomreaktoren, verwendbar in RTG-Baterien
- Tantal - Nötig für Kondensator-Akku und speziel Stoffe
- Tantalhafniumcarbid - Material mit hohen Schmelzpunkt.
- Uran - Radioaktiver Stoff, hocheffizient für Atomreaktoren
- Wolfram - Ein Erz mit hohen Siedepunkt
- Wolframcarbid - Eine Alternative zu Diamanten

#### Hafnium

Hafnium wird verwendet um Steuerstäbe für Atomgeneratoren herzustellen, aber auch um Tantalhafniumcarbid herzustellen.

#### Blei

Blei ist ein schweres Metall. Es eignet sich auch als Abschirmung bei Nutzung von Radioaktivität. Aufgrund seiner Häufigkeit sollte es keine Probleme machen. Sofern nicht anders angegeben zerfallen Materialien die Radioaktiv sind in Blei.

#### Plutonium

Ultra selten, kommt jedoch als Abfallprodukt in Atomreaktoren raus. Kann in RTG-Baterien benutzt werden um Strom zu gewinnen

#### Tantal

Aus Tantal lassen sich Kondensator-Akkus herstellen, die mehr Strom als gewöhnliche Akkus abspeichern können

#### Tantalhafniumcarbid

Tantalhafniumcarbid intermetallische Verbindung aus Tantal, Hafnium und Kohlenstoff. Durch seinen hohen Schmelzpunkt kann es für Röhren verwendet werden die Gas mit hohen Temperaturen durchleiten können.

#### Uran

Uran ist der Brennstoff für Atomkraftwerke, die vor allen Dingen in weiten Formen Mengen an Strom geben.

#### Wolfram

Wolfram hat einen sehr hohen Siedepunkt.

#### Wolframcarbid

Unter anderem eine Alternative zu Diamanten. Außerdem kann man auch Werkzeuge herstellen die mit einer niedrigen Wahrscheinlichkeit Ölröhren beim Abbau wieder geben.


## TA4

### Weitere TA4 Blöcke/Items

#### ESPE TA4 Radionuklidbatterie

Erzeugt Strom aus dem Zerfall von radiokaktiven Materialien.
