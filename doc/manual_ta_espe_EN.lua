techage.add_to_manual('EN', {
  "1,Techage ESPE",
  "2,About",
  "3,Ores and Minerals",
  "4,Hafnium",
  "4,Lead",
  "4,Plutonium",
  "4,Tantalum",
  "4,Tantalum hafnium carbide",
  "4,Uranium",
  "4,Tungsten",
  "4,Tungsten carbide",
}, {
  "TA ESPE\\, short for Techage Energy\\, Storage and Product Extension\\, adds some new things to Techage. (Warning\\, much still planning!)\n"..
  "\n",
  "",
  "TA ESPE adds several new items:\n"..
  "\n"..
  "  - Hafnium - A rare but important material\n"..
  "  - Lead - A heavy metal which is more common\n"..
  "  - Plutonium - Waste product of early nuclear reactors\\, usable in RTG bateries\n"..
  "  - Tantalum - Necessary for capacitor-battery and special materials\n"..
  "  - Tantalum hafnium carbide - Material with high melting point.\n"..
  "  - Uranium - Radioactive material\\, highly efficient for nuclear reactors\n"..
  "  - Tungsten - An ore with high boiling point\n"..
  "  - Tungsten carbide - An alternative to diamonds\n"..
  "\n",
  "Hafnium is used to make control rods for nuclear generators\\, but also to make tantalum hafnium carbide.\n"..
  "\n",
  "Lead is a heavy metal. It is also suitable for shielding when radioactivity is used. Because of its abundance\\, it should not cause problems. Unless otherwise specified\\, materials that are radioactive will decay into lead.\n"..
  "\n",
  "Ultra rare\\, but comes out as a waste product in nuclear reactors. Can be used in RTG batteries to generate electricity.\n"..
  "\n",
  "Tantalum can be used to make battery banks that can store more electricity than ordinary batteries.\n"..
  "\n",
  "Tantalum hafnium carbide intermetallic compound of tantalum\\, hafnium and carbon. Due to its high melting point\\, it can be used for tubes that can pass gas at high temperatures.\n"..
  "\n",
  "Uranium is the fuel for nuclear power plants\\, which produce large amounts of electricity.\n"..
  "\n",
  "Tungsten has a very high boiling point.\n"..
  "\n",
  "Among other things an alternative to diamonds. In addition\\, one can also produce tools that give with a low probability oil tubes when mining.\n"..
  "\n",
}, {
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
}, {
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
  "",
})
