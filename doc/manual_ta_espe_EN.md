# Techage ESPE

TA ESPE, short for Techage Energy, Storage and Product Extension, adds some new things to Techage. (Warning, much still planning!)

## About

### Ores and Minerals

TA ESPE adds several new items:

- Hafnium - A rare but important material
- Lead - A heavy metal which is more common
- Plutonium - Waste product of early nuclear reactors, usable in RTG bateries
- Tantalum - Necessary for capacitor-battery and special materials
- Tantalum hafnium carbide - Material with high melting point.
- Uranium - Radioactive material, highly efficient for nuclear reactors
- Tungsten - An ore with high boiling point
- Tungsten carbide - An alternative to diamonds

#### Hafnium

Hafnium is used to make control rods for nuclear generators, but also to make tantalum hafnium carbide.

#### Lead

Lead is a heavy metal. It is also suitable for shielding when radioactivity is used. Because of its abundance, it should not cause problems. Unless otherwise specified, materials that are radioactive will decay into lead.

#### Plutonium

Ultra rare, but comes out as a waste product in nuclear reactors. Can be used in RTG batteries to generate electricity.

#### Tantalum

Tantalum can be used to make battery banks that can store more electricity than ordinary batteries.

#### Tantalum hafnium carbide

Tantalum hafnium carbide intermetallic compound of tantalum, hafnium and carbon. Due to its high melting point, it can be used for tubes that can pass gas at high temperatures.

#### Uranium

Uranium is the fuel for nuclear power plants, which produce large amounts of electricity.

#### Tungsten

Tungsten has a very high boiling point.

#### Tungsten carbide

Among other things an alternative to diamonds. In addition, one can also produce tools that give with a low probability oil tubes when mining.
