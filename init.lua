ta_espe = {}

ta_espe.protocol_version = 0
ta_espe.version = "0.0"

if minetest.global_exists("techage") and techage.version < 1.08 then
	minetest.log("error", "[ta_espe] TA ESPE requires techage version 1.08 or newer!")
	return
end

ta_espe.S = minetest.get_translator("ta_espe")

local MP = minetest.get_modpath("ta_espe") --Stands for Mod Package
-- Basis
dofile(MP.."/basis/items.lua")
-- Machines
dofile(MP.."/nuclear/rtg.lua")
-- Items
if not minetest.global_exists("technic") then
	dofile(MP.."/items/lead.lua")
end
dofile(MP.."/items/basis_items.lua")
dofile(MP.."/items/electrolyte.lua")
dofile(MP.."/items/plutonium.lua")
dofile(MP.."/items/tungsten.lua")
dofile(MP.."/items/tantalum.lua")
dofile(MP.."/items/uranium.lua")
dofile(MP.."/items/misc.lua")
-- Crafting
dofile(MP.."/crafting/lead.lua")
-- DOCS
dofile(MP.."/doc/manual_ta_espe_DE.lua")
dofile(MP.."/doc/manual_ta_espe_EN.lua")
