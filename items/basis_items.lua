local S = ta_espe.S

minetest.register_craftitem("ta_espe:capacitor", {
	description = S("ESPE TA3 Capacitor"),
	inventory_image = "ta_espe_capacitor.png",
})

local needed_silver_wire

if minetest.global_exists("moreores") then
	needed_silver_wire = "basic_materials:silver_wire"
else
	needed_silver_wire = "basic_materials:copper_wire"-- If moreores not exist it's the replacement
end


techage.recipes.add("ta2_electronic_fab", {
	output = "ta_espe:capacitor 16",
	input = {needed_silver_wire.." 1", "basic_materials:plastic_sheet 1", "techage:usmium_nuggets 1"}
})

techage.recipes.add("ta3_electronic_fab", {
	output = "ta_espe:capacitor 16",
	input = {needed_silver_wire.." 1", "basic_materials:plastic_sheet 1", "techage:usmium_nuggets 1"}
})

techage.recipes.add("ta4_electronic_fab", {
	output = "ta_espe:capacitor 16",
	input = {needed_silver_wire.." 1", "basic_materials:plastic_sheet 1", "techage:usmium_nuggets 1"}
})

minetest.register_craftitem("ta_espe:pure_graphite", {
	description = S("Pure Graphite"),
	inventory_image = "ta_espe_graphite.png",
})

techage.add_grinder_recipe({input="ta_espe:pure_graphite", output="techage:graphite_powder"})

techage.furnace.register_recipe({
	output = "ta_espe:pure_graphite",
	recipe = {
		"default:coal_lump",
	},
	time = 1,
})
