local S = ta_espe.S

minetest.register_craftitem("ta_espe:electrolyte_powder", {
	description = S("Electrolyte Powder"),
	inventory_image = "techage_powder_inv.png^[colorize:#FFFFAA:120",
	groups = {powder = 1},
})

techage.recipes.add("ta4_doser", {
	output = "ta_espe:electrolyte_powder 2",
	waste = "techage:hydrogen 1",
	input = {
		"techage:water 4",
		"techage:usmium_powder 2",
	}
})
