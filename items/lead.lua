local S = ta_espe.S

minetest.register_craftitem("ta_espe:lead_lump", {
	description = S("Lead Lump"),
	inventory_image = "ta_espe_lead_lump.png",
})

minetest.register_craftitem("ta_espe:lead_ingot", {
	description = S("Lead Ingot"),
	inventory_image = "ta_espe_lead_ingot.png",
})

minetest.register_node("ta_espe:leadblock", {
	description = S("Lead Block"),
	tiles = {"ta_espe_lead_block.png"},
	is_ground_content = false,
	groups = {cracky = 3, level = 1},
	sounds = default.node_sound_metal_defaults(),
})

minetest.register_node("ta_espe:stone_with_lead", {
	description = S("Lead Ore"),
	tiles = {"default_stone.png^ta_espe_mineral_lead.png"},
	groups = {cracky = 3},
	drop = 'ta_espe:lead_lump',
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_craft({
	type = 'cooking',
	output = "ta_espe:lead_ingot",
	recipe = "ta_espe:lead_lump",
})

minetest.register_craft({
	output = "ta_espe:leadblock",
	recipe = {
		{"ta_espe:lead_ingot", "ta_espe:lead_ingot", "ta_espe:lead_ingot"},
		{"ta_espe:lead_ingot", "ta_espe:lead_ingot", "ta_espe:lead_ingot"},
		{"ta_espe:lead_ingot", "ta_espe:lead_ingot", "ta_espe:lead_ingot"},
	}
})

minetest.register_craft({
	output = "ta_espe:lead_ingot 9",
	recipe = {
		{"ta_espe:leadblock",},
	}
})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "ta_espe:stone_with_lead",
		wherein        = "default:stone",
		clust_scarcity = 12 * 12 * 12,
		clust_num_ores = 12,
		clust_size     = 3,
		y_max          = 31000,
		y_min          = 1025,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "ta_espe:stone_with_lead",
		wherein        = "default:stone",
		clust_scarcity = 9 * 9 * 9,
		clust_num_ores = 5,
		clust_size     = 3,
		y_max          = -32,
		y_min          = -255,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "ta_espe:stone_with_lead",
		wherein        = "default:stone",
		clust_scarcity = 14 * 14 * 14,
		clust_num_ores = 29,
		clust_size     = 5,
		y_max          = -256,
		y_min          = -31000,
	})
