local S = ta_espe.S

minetest.register_craftitem("ta_espe:iron_disilicide", {
	description = S("Iron disilicide"),
	inventory_image = "techage_iron_ingot.png^[colorize:#000000:155",
})

minetest.register_craft({
	output = "ta_espe:iron_disilicide 3",
	recipe = {
		{"basic_materials:silicon", "techage:iron_ingot", "basic_materials:silicon"},
	},
})
