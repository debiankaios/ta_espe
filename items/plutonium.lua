ta_espe.plutonium_isotopes = {
	--Only for plutonium:
	--ku_per_item = Decay energy(MeV)/MeV*50000
	--halftime isn't true to scale
	[238] = {rarity=0, halftime=120000, ku_per_item=279650,},
	[239] = {rarity=0, halftime=600000, ku_per_item=262250,},
	[243] = {rarity=0, halftime=120, ku_per_item=2895,},--ku_per_item ist ten times deeper then it should be, TODO: still to much
}

local S = ta_espe.S

for i, properties in pairs(ta_espe.plutonium_isotopes) do
	minetest.register_craftitem("ta_espe:plutonium_"..i.."_ingot", {
		description = S("Plutonium-@1 Ingot", i),
		inventory_image = "ta_espe_plutonium_ingot.png",
	})

	minetest.register_node("ta_espe:plutonium_"..i.."_block", {
		description = S("Plutonium-@1 Block", i),
		tiles = {"ta_espe_plutonium_block.png"},
		is_ground_content = false,
		groups = {cracky = 3},
		sounds = default.node_sound_metal_defaults(),
	})

	--Plutonium is good for RTGs
	ta_espe.rtg.register_rtgfuel("ta_espe:plutonium_"..i.."_lump",{
		halftime = properties.halftime/2,
		ku_per_item = properties.ku_per_item/2,
	})

	ta_espe.rtg.register_rtgfuel("ta_espe:plutonium_"..i.."_ingot",{
		halftime = properties.halftime,
		ku_per_item = properties.ku_per_item,
	})

	ta_espe.rtg.register_rtgfuel("ta_espe:plutonium_"..i.."_block",{
		halftime = properties.halftime,
		ku_per_item = properties.ku_per_item*9,
	})
end

for i in pairs(ta_espe.plutonium_isotopes) do
	minetest.register_craft({
		output = "ta_espe:plutonium_"..i.."_block",
		recipe = {
			{"ta_espe:plutonium_"..i.."_ingot", "ta_espe:plutonium_"..i.."_ingot", "ta_espe:plutonium_"..i.."_ingot"},
			{"ta_espe:plutonium_"..i.."_ingot", "ta_espe:plutonium_"..i.."_ingot", "ta_espe:plutonium_"..i.."_ingot"},
			{"ta_espe:plutonium_"..i.."_ingot", "ta_espe:plutonium_"..i.."_ingot", "ta_espe:plutonium_"..i.."_ingot"},
		}
	})

	minetest.register_craft({
		output = "ta_espe:plutonium_"..i.."_ingot 9",
		recipe = {
			{"ta_espe:plutonium_"..i.."_block",},
		}
	})
end
