local S = ta_espe.S

minetest.register_craftitem("ta_espe:tantalum_lump", {
	description = S("Tantalumum Lump"),
	inventory_image = "ta_espe_tantalum_lump.png",
})

minetest.register_craftitem("ta_espe:tantalum_ingot", {
	description = S("Tantalum Ingot"),
	inventory_image = "ta_espe_tantalum_ingot.png",
})

minetest.register_node("ta_espe:tantalumblock", {
	description = S("Tantalum Block"),
	tiles = {"ta_espe_tantalum_block.png"},
	is_ground_content = false,
	groups = {cracky = 2, level = 1},
	sounds = default.node_sound_metal_defaults(),
})

minetest.register_node("ta_espe:stone_with_tantalum", {
	description = S("Tantalum Ore"),
	tiles = {"default_stone.png^ta_espe_mineral_tantalum.png"},
	groups = {cracky = 2},
	drop = 'ta_espe:tantalum_lump',
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_craft({
	type = 'cooking',
	output = "ta_espe:tantalum_ingot",
	recipe = "ta_espe:tantalum_lump",
	cooktime = 10,
})

minetest.register_craft({
	output = "ta_espe:tantalumblock",
	recipe = {
		{"ta_espe:tantalum_ingot", "ta_espe:tantalum_ingot", "ta_espe:tantalum_ingot"},
		{"ta_espe:tantalum_ingot", "ta_espe:tantalum_ingot", "ta_espe:tantalum_ingot"},
		{"ta_espe:tantalum_ingot", "ta_espe:tantalum_ingot", "ta_espe:tantalum_ingot"},
	}
})

minetest.register_craft({
	output = "ta_espe:tantalum_ingot 9",
	recipe = {
		{"ta_espe:tantalumblock",},
	}
})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "ta_espe:stone_with_tantalum",
		wherein        = "default:stone",
		clust_scarcity = 24 * 24 * 24,
		clust_num_ores = 12,
		clust_size     = 3,
		y_max          = 31000,
		y_min          = 1025,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "ta_espe:stone_with_tantalum",
		wherein        = "default:stone",
		clust_scarcity = 22 * 22 * 22,
		clust_num_ores = 5,
		clust_size     = 3,
		y_max          = -128,
		y_min          = -255,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "ta_espe:stone_with_tantalum",
		wherein        = "default:stone",
		clust_scarcity = 25 * 25 * 25,
		clust_num_ores = 29,
		clust_size     = 5,
		y_max          = -256,
		y_min          = -31000,
	})
