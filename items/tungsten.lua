local S = ta_espe.S

minetest.register_craftitem("ta_espe:tungsten_lump", {
	description = S("Tungsten Lump"),
	inventory_image = "ta_espe_tungsten_lump.png",
})

minetest.register_craftitem("ta_espe:tungsten_ingot", {
	description = S("Tungsten Ingot"),
	inventory_image = "ta_espe_tungsten_ingot.png",
})

minetest.register_node("ta_espe:tungstenblock", {
	description = S("Tungsten Block"),
	tiles = {"ta_espe_tungsten_block.png"},
	is_ground_content = false,
	groups = {cracky = 2, level = 1},
	sounds = default.node_sound_metal_defaults(),
})

minetest.register_node("ta_espe:stone_with_tungsten", {
	description = S("Tungsten Ore"),
	tiles = {"default_stone.png^ta_espe_mineral_tungsten.png"},
	groups = {cracky = 2},
	drop = 'ta_espe:tungsten_lump',
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_craft({
	type = 'cooking',
	output = "ta_espe:tungsten_ingot",
	recipe = "ta_espe:tungsten_lump",
	cooktime = 10,
})

minetest.register_craft({
	output = "ta_espe:tungstenblock",
	recipe = {
		{"ta_espe:tungsten_ingot", "ta_espe:tungsten_ingot", "ta_espe:tungsten_ingot"},
		{"ta_espe:tungsten_ingot", "ta_espe:tungsten_ingot", "ta_espe:tungsten_ingot"},
		{"ta_espe:tungsten_ingot", "ta_espe:tungsten_ingot", "ta_espe:tungsten_ingot"},
	}
})

minetest.register_craft({
	output = "ta_espe:tungsten_ingot 9",
	recipe = {
		{"ta_espe:tungstenblock",},
	}
})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "ta_espe:stone_with_tungsten",
		wherein        = "default:stone",
		clust_scarcity = 18 * 18 * 18,
		clust_num_ores = 12,
		clust_size     = 3,
		y_max          = 31000,
		y_min          = 1025,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "ta_espe:stone_with_tungsten",
		wherein        = "default:stone",
		clust_scarcity = 15 * 15 * 15,
		clust_num_ores = 5,
		clust_size     = 3,
		y_max          = -128,
		y_min          = -255,
	})

	minetest.register_ore({
		ore_type       = "scatter",
		ore            = "ta_espe:stone_with_tungsten",
		wherein        = "default:stone",
		clust_scarcity = 20 * 20 * 20,
		clust_num_ores = 29,
		clust_size     = 5,
		y_max          = -256,
		y_min          = -31000,
	})
