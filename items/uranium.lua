ta_espe.uranium_isotopes = {
	[235] = {rarity=0.0075, halftime=600000, ku_per_item=300000,},
	[236] = {rarity=0.0025, halftime=120000, ku_per_item=120000,},
	[238] = {rarity=0.99, halftime=1200000, ku_per_item=120000,},
}

local S = ta_espe.S

for i, properties in pairs(ta_espe.uranium_isotopes) do
	minetest.register_craftitem("ta_espe:uranium_"..i.."_lump", {
		description = S("Uranium-@1 Lump", i),
		inventory_image = "ta_espe_uranium_lump.png",
	})

	minetest.register_craftitem("ta_espe:uranium_"..i.."_ingot", {
		description = S("Uranium-@1 Ingot", i),
		inventory_image = "ta_espe_uranium_ingot.png",
	})

	minetest.register_node("ta_espe:uranium_"..i.."_block", {
		description = S("Uranium-@1 Block", i),
		tiles = {"ta_espe_uranium_block.png"},
		is_ground_content = false,
		groups = {cracky = 3},
		sounds = default.node_sound_metal_defaults(),
	})

	minetest.register_node("ta_espe:stone_with_uranium_"..i, {
		description = S("Uranium-@1 Ore", i),
		tiles = {"default_stone.png^ta_espe_mineral_uranium.png"},
		groups = {cracky = 3},
		drop = "ta_espe:uranium_"..i.."_lump",
		sounds = default.node_sound_stone_defaults(),
	})

	--Uran is weak for RTGs because it isn't thought for it
	ta_espe.rtg.register_rtgfuel("ta_espe:uranium_"..i.."_lump",{
		halftime = properties.halftime/2,
		ku_per_item = properties.ku_per_item/2,
	})

	ta_espe.rtg.register_rtgfuel("ta_espe:uranium_"..i.."_ingot",{
		halftime = properties.halftime,
		ku_per_item = properties.ku_per_item,
	})

	ta_espe.rtg.register_rtgfuel("ta_espe:uranium_"..i.."_block",{
		halftime = properties.halftime,
		ku_per_item = properties.ku_per_item*9,
	})
end

minetest.register_node("ta_espe:uraninite", {
	description = S("Uraninite"),
	tiles = {"default_stone.png^ta_espe_uraninite_overlay.png^[colorize:#000000:100"},
	groups = {cracky = 2, stone = 1},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_node("ta_espe:uraninite_gravel", {
	description = S("Uraninite Gravel"),
	tiles = {"default_gravel.png^ta_espe_uraninite_gravel_overlay.png^[colorize:#000000:140"},
	is_ground_content = false,
	groups = {crumbly = 2, falling_node = 1},
	sounds = default.node_sound_gravel_defaults(),
})

techage.register_stone_gravel_pair("ta_espe:uraninite", "ta_espe:uraninite_gravel")

for i in pairs(ta_espe.uranium_isotopes) do
	minetest.register_craft({
		type = 'cooking',
		output = "ta_espe:uranium_"..i.."_ingot",
		recipe = "ta_espe:uranium_"..i.."_lump",
	})

	minetest.register_craft({
		output = "ta_espe:uranium_"..i.."_block",
		recipe = {
			{"ta_espe:uranium_"..i.."_ingot", "ta_espe:uranium_"..i.."_ingot", "ta_espe:uranium_"..i.."_ingot"},
			{"ta_espe:uranium_"..i.."_ingot", "ta_espe:uranium_"..i.."_ingot", "ta_espe:uranium_"..i.."_ingot"},
			{"ta_espe:uranium_"..i.."_ingot", "ta_espe:uranium_"..i.."_ingot", "ta_espe:uranium_"..i.."_ingot"},
		}
	})

	minetest.register_craft({
		output = "ta_espe:uranium_"..i.."_ingot 9",
		recipe = {
			{"ta_espe:uranium_"..i.."_block",},
		}
	})
end

techage.add_grinder_recipe({input="ta_espe:uraninite", output="ta_espe:uraninite_gravel"})

-- Ores

	minetest.register_ore({
		ore_type        = "blob",
		ore             = "ta_espe:uraninite",
		wherein         = {"default:stone", "default:desert_stone"},
		clust_scarcity  = 24 * 24 * 24,
		clust_size      = 6,
		y_max           = 31000,
		y_min           = -31000,
		noise_threshold = 0.0,
		noise_params    = {
			offset = 0.5,
			scale = 0.2,
			spread = {x = 5, y = 5, z = 5},
			seed = 5737,
			octaves = 1,
			persist = 0.0
		},
	})

	for i, p in pairs(ta_espe.uranium_isotopes) do
		minetest.register_ore({
			ore_type       = "scatter",
			ore            = "ta_espe:stone_with_uranium_"..i,
			wherein        = "default:stone",
			clust_scarcity = math.floor(((45 * 45 * 45)/p.rarity)+0.5),
			clust_num_ores = 12,
			clust_size     = 3,
			y_max          = 31000,
			y_min          = 1025,
		})

		minetest.register_ore({
			ore_type       = "scatter",
			ore            = "ta_espe:stone_with_uranium_"..i,
			wherein        = "default:stone",
			clust_scarcity = math.floor(((40 * 40 * 40)/p.rarity)+0.5),
			clust_num_ores = 24,
			clust_size     = 5,
			y_max          = 128,
			y_min          = -31000,
		})
	end
