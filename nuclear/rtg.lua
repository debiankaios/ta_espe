ta_espe.rtg = {}

local M = minetest.get_meta
local S = ta_espe.S
local T = techage.S

local Cable = techage.ElectricCable
local firebox = techage.firebox
local fuel = techage.fuel
local Pipe = techage.LiquidPipe
local power = networks.power
local liquid = networks.liquid

local CYCLE_TIME = 2
local STANDBY_TICKS = 1
local COUNTDOWN_TICKS = 2
--local PWR_PERF = 1 -- To make new
local EFFICIENCY = 2.5

local pwr_out = 0
local rtg_fuel = {}

function ta_espe.rtg.register_rtgfuel(fuel,def)
	def.halftime = def.halftime or 0 --Time how long it take until half of energy was given in seconds. If there are under 0.1 items energy stops to generate
	def.ku_per_item = def.ku_per_item or 0 --Total ku the item give.
	rtg_fuel[fuel] = def
end

local function formspec(self, pos, nvm)
	local fuel_percent = 0
	if nvm.used_item then
		fuel_percent = ((nvm.remained_items - math.floor(nvm.remained_items) or 1) * 100)
	end
	return "size[8,9]"..
		default.gui_bg..
		default.gui_bg_img..
		default.gui_slots..
		"box[0,-0.1;4.8,0.5;#c6e8ff]"..
		"label[0.2,-0.1;"..minetest.colorize("#000000", S("Radionuclide generator")).."]"..
		--fuel.fuel_container(0, 0.9, nvm)..
		"list[current_name;fuel;0,2.1;1,1;]"..
		"image[0,0.9;1,1;default_furnace_fire_bg.png^[lowpart:"..
		fuel_percent..":default_furnace_fire_fg.png]"..
		"image[1.4,1.6;1,1;techage_form_arrow_bg.png^[transformR270]"..
		"image_button[1.4,3.2;1,1;".. self:get_state_button_image(nvm) ..";state_button;]"..
		"tooltip[1.5,3;1,1;"..self:get_state_tooltip(nvm).."]"..
		"list[current_player;main;0,4.5;8,4;]"..
		techage.formspec_power_bar(pos, 2.5, 0.8, T("Electricity"), nvm.provided, nvm.pwr_out)
end

local function elemaking(pos, nvm)
	local inv = M(pos):get_inventory()
	local stack = inv:get_stack("fuel", 1)
	local item_name = stack:get_name()
	nvm.last_item_count = nvm.last_item_count or 0
	if nvm.used_item then
		if not (item_name == nvm.used_item) then
			nvm.remained_items = nvm.remained_items - math.floor(nvm.remained_items)
		end
		if nvm.pwr_out < 0.1 then
			--nvm.pwr_out = rtg_fuel[nvm.used_item].ku_per_item*0.1
			nvm.used_item = false
			nvm.remained_items = 0
		else
			nvm.remained_items = nvm.remained_items*2^-(CYCLE_TIME/rtg_fuel[nvm.used_item].halftime)
		end
	end
	for fuel,def in pairs(rtg_fuel) do
		if item_name == fuel then
			if nvm.used_item then
				if item_name == nvm.used_item then
					if stack:get_count() > nvm.last_item_count then
						nvm.remained_items = nvm.remained_items + stack:get_count() - nvm.last_item_count
					end
					if nvm.remained_items < stack:get_count() then
						stack:take_item()
					end
					nvm.remained_items = nvm.remained_items - math.floor(nvm.remained_items) + stack:get_count()
					stack:set_count(math.floor(nvm.remained_items))
					inv:set_stack("fuel", 1, stack)
				end
			else
				nvm.used_item = item_name
				nvm.remained_items = stack:get_count()
				nvm.remained_items = nvm.remained_items*2^-(CYCLE_TIME/rtg_fuel[nvm.used_item].halftime)
				stack:set_count(math.floor(nvm.remained_items))
				inv:set_stack("fuel", 1, stack)
			end
		end
	end
	if nvm.used_item then
		nvm.pwr_out = rtg_fuel[nvm.used_item].ku_per_item*(nvm.remained_items-(nvm.remained_items*2^-(CYCLE_TIME/rtg_fuel[nvm.used_item].halftime)))
	end
	nvm.last_item_count = stack:get_count()
end

function has_fuel(pos, nvm)
	local inv = M(pos):get_inventory()
	local items = inv:get_stack("fuel", 1)
	local has_rtg_fuel = false
	for fuel in pairs(rtg_fuel) do
		has_rtg_fuel = has_rtg_fuel or items:get_name() == fuel
	end
	return nvm.used_item or has_rtg_fuel
end

local function can_start(pos, nvm, state)
	if has_fuel(pos, nvm) then
		return true
	end
	return T("no fuel")
end

local function start_node(pos, nvm, state)
	local meta = M(pos)
	nvm.provided = 0
	local outdir = meta:get_int("outdir")
	power.start_storage_calc(pos, Cable, outdir)
	techage.evaluate_charge_termination(nvm, meta)
end

local function stop_node(pos, nvm, state)
	nvm.provided = 0
	local outdir = M(pos):get_int("outdir")
	power.start_storage_calc(pos, Cable, outdir)
end

local State = techage.NodeStates:new({
	node_name_passive = "ta_espe:rtg",
	node_name_active = "ta_espe:rtg_on",
	cycle_time = CYCLE_TIME,
	standby_ticks = STANDBY_TICKS,
	formspec_func = formspec,
	infotext_name = S("ESPE TA4 Radionuclide generator"),
	can_start = can_start,
	start_node = start_node,
	stop_node = stop_node,
})

local function node_timer(pos, elapsed)
	local nvm = techage.get_nvm(pos)
	local running = techage.is_running(nvm)
	local fuel = has_fuel(pos, nvm)
	if running and not fuel then
		State:standby(pos, nvm, T("no fuel"))
		stop_node(pos, nvm, State)
	elseif not running and fuel then
		State:start(pos, nvm)
		-- start_node() is called implicit
	elseif running then
		local meta = M(pos)
		local outdir = meta:get_int("outdir")
		elemaking(pos, nvm)
		nvm.provided = power.provide_power(pos, Cable, outdir, nvm.pwr_out)
		local val = power.get_storage_load(pos, Cable, outdir, nvm.pwr_out)
		if val > 0 then
			nvm.load = val
		end
		State:keep_running(pos, nvm, COUNTDOWN_TICKS)
	else
		if nvm.used_item then
			nvm.remained_items = nvm.remained_items*2^-(CYCLE_TIME/rtg_fuel[nvm.used_item].halftime)
		end
	end
	if techage.is_activeformspec(pos) then
		M(pos):set_string("formspec", formspec(State, pos, nvm))
	end
	return State:is_active(nvm)
end

local function on_receive_fields(pos, formname, fields, player)
	if minetest.is_protected(pos, player:get_player_name()) then
		return
	end
	local nvm = techage.get_nvm(pos)
	State:state_button_event(pos, nvm, fields)
end

local function on_rightclick(pos, node, clicker)
	local nvm = techage.get_nvm(pos)
	techage.set_activeformspec(pos, clicker)
	M(pos):set_string("formspec", formspec(State, pos, nvm))
end

local allow_metadata_inventory_move = function(pos, from_list, from_index, to_list, to_index, count, player)
	if minetest.is_protected(pos, player:get_player_name()) then
		return 0
	end
	return stack:get_count()
end

local allow_metadata_inventory_put = function(pos, listname, index, stack, player)
	if minetest.is_protected(pos, player:get_player_name()) then
		return 0
	end
	return stack:get_count()
end

local allow_metadata_inventory_take = function(pos, listname, index, stack, player)
	if minetest.is_protected(pos, player:get_player_name()) then
		return 0
	end
	return stack:get_count()
end

minetest.register_node("ta_espe:rtg", {
	description = S("ESPE TA4 Radionuclide generator"),
	tiles = {
		-- up, down, right, left, back, front
		"techage_appl_electric_gen_top.png^techage_frame_ta4_top.png",
		"techage_appl_electric_gen_top.png^techage_frame_ta4.png",
		"techage_appl_electric_gen_side.png^techage_appl_hole_electric.png^techage_frame_ta4.png",
		"techage_appl_electric_gen_side.png^techage_frame_ta4.png",
		"techage_appl_electric_gen_front.png^[transformFX]^techage_frame_ta4.png",
		"techage_appl_electric_gen_front.png^techage_frame_ta4.png",
	},
	paramtype2 = "facedir",
	groups = {cracky=2, crumbly=2, choppy=2},
	on_rotate = screwdriver.disallow,
	is_ground_content = false,

	after_place_node = function(pos, placer, itemstack)
		local nvm = techage.get_nvm(pos)
		local number = techage.add_node(pos, "ta_espe:rtg")
		nvm.pwr_out = 0
		M(pos):get_inventory():set_size("fuel", 1)
		State:node_init(pos, nvm, number)
		M(pos):set_string("formspec", formspec(State, pos, nvm))
		M(pos):set_int("outdir", networks.side_to_outdir(pos, "R"))
		Pipe:after_place_node(pos)
		Cable:after_place_node(pos)
	end,

	after_dig_node = function(pos, oldnode)
		Cable:after_dig_node(pos)
		techage.del_mem(pos)
	end,

	--[[preserve_metadata = function(pos, oldnode, oldmetadata, drops)
		local nvm = techage.get_nvm(pos)
		if nvm.liquid and nvm.liquid.name and nvm.liquid.amount > 0 then
			-- generator tank is not empty
			local meta = drops[1]:get_meta()
			meta:set_string("liquid_name", nvm.liquid.name)
			meta:set_int("liquid_amount", nvm.liquid.amount)
			meta:set_string("description", !!!translator!!!("TA3 Tiny Power Generator") .. " (fuel: " ..
			                tostring(nvm.liquid and nvm.liquid.amount or 0) .. "/" ..
					tostring(fuel.CAPACITY) .. ")")
		end
	end,]]

	--get_generator_data = get_generator_data,
	on_receive_fields = on_receive_fields,
	on_rightclick = on_rightclick,
	on_timer = node_timer,
	allow_metadata_inventory_move = allow_metadata_inventory_move,
	allow_metadata_inventory_put = allow_metadata_inventory_put,
	allow_metadata_inventory_take = allow_metadata_inventory_take,
})

minetest.register_node("ta_espe:rtg_on", {
	description = S("ESPE TA4 Radionuclide generator"),
	tiles = {
		-- up, down, right, left, back, front
		"techage_appl_electric_gen_top.png^techage_frame_ta4_top.png",
		"techage_appl_electric_gen_top.png^techage_frame_ta4.png",
		"techage_appl_electric_gen_side.png^techage_appl_hole_electric.png^techage_frame_ta4.png",
		"techage_appl_electric_gen_side.png^techage_frame_ta4.png",
		{
			image = "techage_appl_electric_gen_front4.png^[transformFX]^techage_frame4_ta4.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 32,
				aspect_h = 32,
				length = 0.8,
			},
		},
		{
			image = "techage_appl_electric_gen_front4.png^techage_frame4_ta4.png",
			backface_culling = false,
			animation = {
				type = "vertical_frames",
				aspect_w = 32,
				aspect_h = 32,
				length = 0.8,
			},
		},
	},

	paramtype = "light",
	paramtype2 = "facedir",
	groups = {not_in_creative_inventory=1},
	diggable = false,
	light_source = 4,
	on_rotate = screwdriver.disallow,
	is_ground_content = false,
	--get_generator_data = get_generator_data,
	on_receive_fields = on_receive_fields,
	on_rightclick = on_rightclick,
	on_timer = node_timer,
	--can_dig = fuel.can_dig,
	allow_metadata_inventory_move = allow_metadata_inventory_move,
	allow_metadata_inventory_put = allow_metadata_inventory_put,
	allow_metadata_inventory_take = allow_metadata_inventory_take,
})

power.register_nodes({"ta_espe:rtg", "ta_espe:rtg_on"}, Cable, "gen", {"R"})

techage.register_node({"techage:tiny_generator", "techage:tiny_generator_on"}, {
	--[[on_node_load = function(pos, node)
		State:on_node_load(pos)
		if node.name == "techage:tiny_generator_on" then
			play_sound(pos)
		end
		local inv = M(pos):get_inventory()
		if not inv:is_empty("fuel") then
			local nvm = techage.get_nvm(pos)
			nvm.liquid = nvm.liquid or {}
			local count = inv:get_stack("fuel", 1):get_count()
			nvm.liquid.amount = (nvm.liquid.amount or 0) + count
			nvm.liquid.name = "techage:gasoline"
			inv:set_stack("fuel", 1, nil)
		end
	end,]]
})


minetest.register_craft({
	output = "ta_espe:rtg",
	recipe = {
		{ta_espe.items.lead_ingot, "techage:usmium_nuggets", ta_espe.items.lead_ingot},
		{"dye:blue", "ta_espe:iron_disilicide", "techage:electric_cableS"},
		{ta_espe.items.lead_ingot, "techage:ta4_wlanchip", ta_espe.items.lead_ingot},
	},
})
